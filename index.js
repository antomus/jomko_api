var express = require('express');
var mongoose = require('mongoose');
//mongoose.set('debug', true);
// Mongoose connection to MongoDB (ted/ted is readonly)
mongoose.connect('mongodb://127.0.0.1:27017/jomko_api', function (error) {
  if (error) {
    console.log(error);
  }
});

// Mongoose Schema definition
var Schema = mongoose.Schema;
var PointSchema = new Schema({
  raw: String,
  datetime: Number,
  latitude: Number, 
  longitude: Number,
  imei: Number
});

// Mongoose Model definition
var Point = mongoose.model('points', PointSchema);

// Bootstrap express
var app = express();


var prepareFilter = function(filter, req) {

    if(req.query.from) {
      filter.datetime = {"$gte": req.query.from};
    }

    if(req.query.to) {
      if(filter.datetime) {
        filter.datetime['$lte'] = req.query.to;
      } else {
        filter.datetime = {"$lte": req.query.to};
      }
    }

    return filter;
}

// URLS management
app.get('/points', function (req, res) {
  Point.find(prepareFilter({}, req), function (err, docs) {
    res.json(docs);
  });
});

app.get('/points/:imei', function (req, res) {
  if (req.params.imei) {
    Point.find(prepareFilter({imei: req.params.imei}, req), function (err, docs) {
      res.json(docs);
    });
  }
});



var server = app.listen(8080, function () {

  var host = server.address().address
  var port = server.address().port

  console.log('App listening at http://%s:%s', host, port)

});